<?php

/**
 * Implementation of hook_default_fe_block_boxes().
 */
function newsfeed_page_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass;
  $fe_block_boxes->info = 'My Room';
  $fe_block_boxes->format = '7';
  $fe_block_boxes->machine_name = 'my_room';
  $fe_block_boxes->body = '<?php
  global $user;
  if($user->uid == 0) return;
  $result = db_query("SELECT COUNT(*) as c FROM user_relationships UR LEFT JOIN users U on UR.requestee_id = U.uid WHERE (UR.approved = \'1\') AND (UR.requester_id = \'%d\') ", $user->uid);
  $row = db_fetch_array($result);
  $following = $row[\'c\'];
  $result = db_query("SELECT COUNT(*) as c FROM user_relationships UR LEFT JOIN users U on UR.requestee_id = U.uid WHERE (UR.approved = \'1\') AND (UR.requestee_id = \'%d\') ", $user->uid);
  $row = db_fetch_array($result);
  $follower = $row[\'c\'];
  ?>
  <div class=\'myroom\'>
	<div class="header">
        </div>
        <div class="items">
		<div class="item first"><?php print(l(\'View My Profile\', "user/{$user->uid}"))?></div>
        	<div class="item"><div class="number"><?php print($following) ?></div><div class="text">Following</div></div>
		<div class="item last"><div class="number"><?php print($follower) ?></div><div class="text">Follower</div></div>
        </div>
  </div> 
 ';

  $export['my_room'] = $fe_block_boxes;

  return $export;
}
