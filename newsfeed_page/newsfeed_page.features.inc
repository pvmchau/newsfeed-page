<?php

/**
 * Implementation of hook_views_api().
 */
function newsfeed_page_views_api() {
  return array(
    'api' => '3.0',
  );
}
