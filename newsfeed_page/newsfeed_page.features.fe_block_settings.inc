<?php

/**
 * Implementation of hook_default_fe_block_settings().
 */
function newsfeed_page_default_fe_block_settings() {
  $export = array();

  // academicroom
  $theme = array();

  $theme['views-my_disciplines-my_disciplines'] = array(
    'module' => 'views',
    'delta' => 'my_disciplines-my_disciplines',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-124',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_1'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_1',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-122',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_2'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_2',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-121',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_3'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_3',
    'theme' => 'academicroom',
    'status' => '1',
    'weight' => '-123',
    'region' => 'right',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $export['academicroom'] = $theme;

  // rubik
  $theme = array();

  $theme['views-my_disciplines-my_disciplines'] = array(
    'module' => 'views',
    'delta' => 'my_disciplines-my_disciplines',
    'theme' => 'rubik',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_1'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_1',
    'theme' => 'rubik',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_2'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_2',
    'theme' => 'rubik',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_3'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_3',
    'theme' => 'rubik',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $export['rubik'] = $theme;

  // garland
  $theme = array();

  $theme['views-my_disciplines-my_disciplines'] = array(
    'module' => 'views',
    'delta' => 'my_disciplines-my_disciplines',
    'theme' => 'garland',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_1'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_1',
    'theme' => 'garland',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_2'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_2',
    'theme' => 'garland',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $theme['views-news_feed_blocks-block_3'] = array(
    'module' => 'views',
    'delta' => 'news_feed_blocks-block_3',
    'theme' => 'garland',
    'status' => 0,
    'weight' => '0',
    'region' => '',
    'custom' => '0',
    'throttle' => '0',
    'visibility' => '1',
    'pages' => 'newsfeed',
    'title' => '',
    'cache' => '-1',
  );

  $export['garland'] = $theme;

  $theme_default = variable_get('theme_default', 'garland');
  $themes = list_themes();
  foreach ($export as $theme_key => $settings) {
    if ($theme_key != $theme_default && empty($themes[$theme_key]->status)) {
      unset($export[$theme_key]);
    }
  }
  return $export;
}
